#!/bin/bash

lagi='y'
while [ $lagi = 'y' ]
do

    echo "-----------------------------------------"
    echo " POGRAM PENDETEKSI BILANGAN GANJIL GENAP"
    echo "-----------------------------------------"

    echo -n "Masukan Angka:"
    read angka
    sisa=$((angka%2))
    if  [ $sisa -eq 0 ]
    then
	    echo "$angka Adalah Bilangan Genap"
    else
	    echo "$angka Adalah Bilangan Ganjil"
    fi 
    echo "============================================="
    echo -n "Mau Coba Lagi? (Y/N): "
    read lagi;
done
