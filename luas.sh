#!/bin/bash
echo "========================="
echo "        Pilih Menu"
echo "========================="
echo "1. Hitung Luas Segitiga"
echo "2. Hitung Luas Lingkaran"
echo "Pilih Menu :"
read pilih

case $pilih in
1)
    echo "========================="
    echo "  Hitung Luas Segitiga"
    echo "========================="
    echo "Input Alas : "
    read a
    echo "Input tinggi: "
    read t
    echo "========================="
    k=`echo 0.5*$a*$t |bc`
    echo "Luas Segitiga = $k"
    ;;
2)
    echo "========================="
    echo " Hitung Luas Lingkaran"
    echo "========================="
    echo "Input Jari-jari lingkaran: "
    read r
    echo "========================="
    k=`echo 3.14*$r*$r |bc`
    echo "Luas lingkaran = $k"
    ;;
*)
    echo "Pilihan Salah"
    esac
